# Assignment for the freelancer

## Steps to follow

1. Create Revo tables/zones models
2. Create getRevoTables function (index.ts)
3. Create unit tests for getRevoTables function
4. The freelancer will set up a server with an endpoint (POST) which will listen to queries from the outside (getTables)
5. The freelancer will set up an authorization via a Dummy API Token

## Delivery (48 hours from the reception of the assignment)

-   The freelancer will provide the code for an Express Server using Typescript
-   The freelancer will provide documentation for connecting to the endpoint (token and options)
-   The freelancer will provide the repository containing the freelancer's code

> Note: There is valuable info in the .env file

# Resolution brief

For run this exercise:

1. Have NodeJS installed
2. With the current directory in the project folder install dependencies with `npm i`
3. Scripts:
    - Test: `npm run test`
    - Dev Server: `npm run dev`
    - Build dist: `npm run build`
    - Run dist bundle: `npm start`
    - Remove dist folder: `npm run clean`

## API docs

### Authorization token

An authorization bearer is impplemented.

> **The dummy token is `lucaluca`.**

### Endpoints

All api endpoints will have `/api` as prefix.

-   `/api/getTables`: is the function that runs `getRevoTables` function of the `Revo` service

## Folder structure

I decided to write the code responsible for connecting to the Revo API and modeling the response inside the `src/services/revo folder`. The Revo folder contains a class called `Revo` which can scale to accommodate any actions involving the Revo API. My goal was to implement an architecture that favors decoupling and extensibility of the project.

## getRevoTables

The primary function of this exercise, `getRevoTables`, is a method of the `Revo` service object located in `src/services/revo/index.ts`. This method calls the getRooms function from the service repository, then `getRevoTables` maps the information to the required models and returns it.

### Test

The unit test is written with Jest in `src/services/revo/index.test.ts`. The test expects the method to return the correct data schema. Additionally, the test mocks the repository dependency that `getRevoTables` calls to fetch API data. This approach allows the test to specifically focus on the `getRevoTables` method, independent of external API responses.
