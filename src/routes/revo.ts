import { Router } from "express"
import revoController from "../controllers/revoController"

const routes = Router()

routes.get("/revo", (_req, res) => res.json("REVO ROUTES"))
routes.post("/getTables", revoController.tablesController)

export default routes
