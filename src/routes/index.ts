import { Router } from "express"
import revoRoutes from "./revo"
import { authMiddleware } from "../middlewares/auth"

const routes = Router()

routes.use(authMiddleware, revoRoutes)
routes.get("/", (_req, res) => res.send("integrations-assignment"))

export default routes
