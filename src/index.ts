import { createApp } from "./app"
import { startServer } from "./server"
;(() => {
    try {
        const app = createApp()
        startServer(app)
    } catch (err) {
        console.log(err)
        process.exit(1)
    }
})()
