import express, { type Application } from "express"
import morgan from "morgan"
import cors from "cors"
import routes from "../routes"
import CONFIG from "../config"
import { notFound, globalError } from "../middlewares/errorsHandlers"

const { PREFIX } = CONFIG.APP

export const createApp = (): Application => {
    const app = express()

    app.use(morgan("dev"))
    app.use(cors())
    app.use(PREFIX, routes)
    app.use(globalError)
    app.use(notFound)

    return app
}
