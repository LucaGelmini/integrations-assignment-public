import CONFIG from "../../config"

const headers: RequestInit["headers"] = {
    tenant: CONFIG.REVO.REVO_TENANT,
    Authorization: `Bearer ${CONFIG.REVO.REVO_TOKEN}`,
    "client-token": CONFIG.REVO.REVO_CLIENT_TOKEN,
    "Accept-Language": "application/json"
}
const baseURL = CONFIG.REVO.REVO_URL

type ResponseData<T = undefined> = {
    current_page: number
    data: T
    first_page_url: string
    from: number
    last_page: number
    last_page_url: "https://integrations.revoxef.works/api/external/v2/rooms?page=1"
    links: { url: string | null; label: string; active: boolean }[]
    next_page_url: string | null
    path: string
    per_page: number
    prev_page_url: string | null
    to: number
    total: number
}

const httpActions = {
    get: async <T = undefined>(
        endpoint: string,
        params?: Record<string, any>
    ): Promise<ResponseData<T>> => {
        const fullURL = new URL(endpoint, baseURL)
        if (params) {
            for (let param in params) {
                fullURL.searchParams.append(param, params[param])
            }
        }
        const res = await fetch(fullURL, {
            method: "GET",
            headers
        })
        if (res.status < 200 || res.status > 200) {
            throw Error(`HTTP error ${res.status}: ${res.statusText}`)
        }
        const data: unknown = await res.json()
        return data as ResponseData<T>
    },
    post: async <T = undefined>(
        endpoint: string,
        body: Record<string, any>
    ): Promise<ResponseData<T>> => {
        const fullURL = new URL(endpoint, baseURL).toString()
        const res = await fetch(fullURL, {
            method: "POST",
            headers,
            body: JSON.stringify(body)
        })
        const data: unknown = await res.json()
        return data as ResponseData<T>
    },
    put: async (): Promise<any> => null,
    patch: async (): Promise<any> => null,
    delete: async (): Promise<any> => null
}

export default httpActions
