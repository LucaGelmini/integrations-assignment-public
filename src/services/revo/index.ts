import { FoodlusZoneModel } from "../../models/tables"
import repository from "./repository"

class Revo {
    async getRevoTables(): Promise<FoodlusZoneModel[]> {
        const rooms = await repository.getRooms()
        return rooms.map(({ id, name, tables }) => {
            const serviceLocations = tables?.map((table) => {
                return {
                    name: table.name,
                    code: table.id,
                    zoneId: id,
                    zoneName: name
                }
            })
            const zone = {
                name,
                serviceLocations
            }

            return zone
        })
    }
}

export default Revo
