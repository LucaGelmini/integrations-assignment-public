import Revo from "."
import repository from "./repository"

jest.mock("./repository")
const mockGetRooms = repository.getRooms as jest.MockedFunction<
    typeof repository.getRooms
>
afterEach(() => {
    jest.clearAllMocks()
})

it("returns an array of Foodlus zones with its tables", async () => {
    mockGetRooms.mockResolvedValue([
        {
            id: 1,
            name: "Room 1",
            order: "0",
            active: "1",
            tables: [
                {
                    id: 1,
                    name: "Table 1",
                    x: "0",
                    y: "0",
                    width: "100",
                    height: "100",
                    baseX: "0",
                    baseY: "0",
                    isJoined: "0",
                    baseWidth: "100",
                    baseHeight: "100",
                    color: "#dddddd",
                    type_id: "2",
                    room_id: "1",
                    price_id: null
                }
            ]
        }
    ])

    const revoService = new Revo()
    const zones = await revoService.getRevoTables()

    expect(Array.isArray(zones)).toBe(true)

    zones.forEach((zone) => {
        expect(zone).toHaveProperty("name")
        expect(typeof zone.name).toBe("string")

        expect(zone).toHaveProperty("serviceLocations")
        expect(Array.isArray(zone.serviceLocations)).toBe(true)

        zone.serviceLocations.forEach((table) => {
            expect(table).toHaveProperty("name")
            expect(typeof table.name).toBe("string")

            expect(table).toHaveProperty("code")
            expect(typeof table.code).toBe("number")

            expect(table).toHaveProperty("zoneId")
            expect(
                typeof table.zoneId === "number" ||
                    typeof table.zoneId === "string"
            ).toBeTruthy()

            expect(table).toHaveProperty("zoneName")
            expect(typeof table.zoneName).toBe("string")
        })
    })
})
