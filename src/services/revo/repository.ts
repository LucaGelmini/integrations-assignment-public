import api from "./api"
import type { Room } from "./apiResponsesModel"

const repository = {
    getRooms: async () => {
        const { data } = await api.get<Room[]>("api/external/v2/rooms", {
            withTables: true
        })
        return data
    }
}

export default repository
