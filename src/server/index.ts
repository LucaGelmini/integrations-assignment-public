import type { Application } from "express"
import CONFIG from "../config"

export const startServer = (app: Application) => {
    app.listen({ port: CONFIG.SERVER.PORT }, () => {
        process.stdout.write(
            `✨✨✨\nServer listening: ${CONFIG.SERVER.HOST}:${CONFIG.SERVER.PORT}\n✨✨✨\n}`
        )
    })
}
