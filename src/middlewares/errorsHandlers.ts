import HttpStatus, { NOT_FOUND, INTERNAL_SERVER_ERROR } from "http-status/lib"
import { HttpError } from "../exceptions"
import type { Request, Response, NextFunction } from "express"

export const notFound = (
    _req: Request,
    res: Response,
    _next: NextFunction
): void => {
    res.status(NOT_FOUND).json(new HttpError(NOT_FOUND))
}

export const globalError = (
    err: any,
    _req: Request,
    res: Response,
    _next: NextFunction
): void => {
    const statusCode: number = HttpStatus[err.statusCode]
        ? err.statusCode
        : INTERNAL_SERVER_ERROR

    const body = {
        statusCode,
        message: err.message
    }

    res.status(statusCode).json(body)
}
