import type { Request, Response, NextFunction } from "express"
import CONFIG from "../config"
import { HttpError } from "../exceptions"
import { UNAUTHORIZED } from "http-status"

export const authMiddleware = (
    req: Request,
    _res: Response,
    next: NextFunction
): void => {
    const authorization = req.headers.authorization || ""
    const token = authorization.split("Bearer ")[1]
    const isAuthorized = token === CONFIG.APP.DUMMY_TOKEN

    if (!token || !isAuthorized) {
        throw new HttpError(UNAUTHORIZED)
    }
    next()
}
