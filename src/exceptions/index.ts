import HttpStatus from "http-status/lib"

export class HttpError extends Error {
    public readonly statusCode: number
    public readonly message: string

    constructor(errorCode: number) {
        super()
        this.statusCode = errorCode
        const message = HttpStatus[errorCode]
        if (!message) {
            throw Error(`Invalid error code: ${errorCode}`)
        }
        this.message = message

        Error.captureStackTrace(this)
    }
}
