require("dotenv").config()

const currentEnv = process.env.NODE_ENV || "development"

const CONFIG = {
    REVO: {
        REVO_TENANT: process.env.REVO_TENANT || "",
        REVO_TOKEN: process.env.REVO_TOKEN || "",
        REVO_URL: process.env.REVO_URL || "",
        REVO_CLIENT_TOKEN: process.env.REVO_CLIENT_TOKEN || ""
    },
    SERVER: {
        HOST:
            currentEnv === "development"
                ? "http://127.0.0.1"
                : process.env.REVO_URL,
        PORT: "3000"
    },
    APP: {
        PREFIX: "/api",
        DUMMY_TOKEN: process.env.API_DUMMY_TOKEN
    }
}

export default CONFIG
