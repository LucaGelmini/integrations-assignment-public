import type { Response, Request } from "express"
import Revo from "../services/revo"

export default {
    tablesController: async (_req: Request, res: Response) => {
        const revoService = new Revo()
        const rooms = await revoService.getRevoTables()
        res.json(rooms)
    }
}
